using System.Collections.Generic;
using System.Threading.Tasks;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public interface IResourceService
    {
        public Task<ResourceDTO> GetResourceById(int resourceId, int workspaceId);

        public Task<IEnumerable<TabDTO>> GetTabsByResourceId(int resourceId, int workspaceId);

        public Task<ResourceDTO> CreateResource(PostResourceDTO resourceDTO);

        public Task<ResourceDTO> UpdateResourceName(int resourceId, PutNameResourceDTO resourceDTO);

        public Task<ResourceDTO> UpdateResourceWorkspaceId(int resourceId, int workspaceId, PutWorkspaceIdResourceDTO resourceDTO);

        public Task DeleteResource(int resourceId);
    }
}