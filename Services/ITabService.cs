using System.Collections.Generic;
using System.Threading.Tasks;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public interface ITabService
    {
        public Task<TabDTO> GetTabById(int tabId, int resourceId, int workspaceId);

        public Task<TabDTO> CreateTab(PostTabDTO tabDTO);

        public Task<TabDTO> UpdateTabLink(int tabId, int resourceId, int workspaceId, PutLinkTabDTO tabDTO);

        public Task<TabDTO> UpdateTabResourceId(int tabId, int resourceId, int workspaceId, PutResourceIdTabDTO tabDTO);

        public Task DeleteTab(int tabId, int resourceId, int workspaceId);
    }
}