using System.Collections.Generic;
using System.Threading.Tasks;

using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public interface IUserService
    {
        public Task<User> GetUserById(int userId);

        public Task<IEnumerable<WorkspaceDTO>> GetWorkspacesByUserId(int userId, AccessLevel? accessLevel = null);

        // public Task CreateWorkspaceForUser(int userId, Workspace workspace);

        // public Task<Workspace> GivePermissionToUser(int receivingUserId, int workspaceId, AccessLevel accessLevelGranted);
    }
}
