using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

using AutoMapper;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{

    public class TabService : ITabService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<TabService> _log;

        public TabService(
            AppDbContext context,
            IMapper mapper,
            ILogger<TabService> log)
        {
            _context = context;
            _mapper = mapper;
            _log = log;
        }

        public async Task<TabDTO> GetTabById(int tabId, int resourceId, int workspaceId)
        {
            var ws = await _context.Workspaces
                .AsNoTracking()
                .Include(ws => ws.Resources)
                .ThenInclude(rs => rs.Tabs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um RecursoId:{resourceId}.");
            var tb = rs.Tabs.SingleOrDefault(tb => tb.TabId == tabId);
            if (tb == null) throw new NullReferenceException($"Não existe uma Tab com a TabId:{tabId}.");
            return _mapper.Map<TabDTO>(tb);
        }

        public async Task<TabDTO> CreateTab(PostTabDTO tabDTO)
        {
            var ws = await _context.Workspaces
                .Include(ws => ws.Resources)
                .ThenInclude(rs => rs.Tabs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == tabDTO.WorkspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == tabDTO.ResourceId);
            if (rs == null) throw new NullReferenceException($"Não é possível criar uma Tab nesse ResourceId:{tabDTO.ResourceId}.");
            var tb = new Tab()
            {
                Link = tabDTO.Link,
                ResourceId = tabDTO.ResourceId
            };
            rs.Tabs.Add(tb);
            await _context.SaveChangesAsync();
            return _mapper.Map<TabDTO>(tb);
        }

        public async Task<TabDTO> UpdateTabLink(int tabId, int resourceId, int workspaceId, PutLinkTabDTO tabDTO)
        {
            var ws = await _context.Workspaces
                .Include(ws => ws.Resources)
                .ThenInclude(rs => rs.Tabs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um RecursoId:{resourceId}.");
            var tb = rs.Tabs.SingleOrDefault(tb => tb.TabId == tabId);
            if (tb == null) throw new NullReferenceException($"Não existe um TabId:{tabId}.");
            tb.Link = tabDTO.Link;
            await _context.SaveChangesAsync();
            return _mapper.Map<TabDTO>(tb);
        }

        public async Task<TabDTO> UpdateTabResourceId(int tabId, int resourceId, int workspaceId, PutResourceIdTabDTO tabDTO)
        {
            var ws = await _context.Workspaces
             .Include(ws => ws.Resources)
             .ThenInclude(rs => rs.Tabs)
             .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um RecursoId:{resourceId}.");
            var tb = rs.Tabs.SingleOrDefault(tb => tb.TabId == tabId);
            if (tb == null) throw new NullReferenceException($"Não existe uma Tab com a TabId:{tabId}.");
            tb.ResourceId = tabDTO.ResourceId;
            await _context.SaveChangesAsync();
            return _mapper.Map<TabDTO>(tb);
        }

        public async Task DeleteTab(int tabId, int resourceId, int workspaceId)
        {
            var ws = await _context.Workspaces
                .Include(ws => ws.Resources)
                .ThenInclude(rs => rs.Tabs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um RecursoId:{resourceId}.");
            var tb = rs.Tabs.SingleOrDefault(tb => tb.TabId == tabId);
            if (tb == null) throw new NullReferenceException($"Não existe um Recurso com a ResourceId:{resourceId}.");
            rs.Tabs.Remove(tb);
            await _context.SaveChangesAsync();
        }
    }

}