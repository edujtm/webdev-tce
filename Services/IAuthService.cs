
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public interface IAuthService
    {
        public bool CheckPassword(User user, string password);
        public AuthResult CreateUser(User user, string password);
        public User FindUserByName(string username);
    }
}
