using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

using AutoMapper;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{

    public class ResourceService : IResourceService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<ResourceService> _log;

        public ResourceService(
            AppDbContext context,
            IMapper mapper,
            ILogger<ResourceService> log)
        {
            _context = context;
            _mapper = mapper;
            _log = log;
        }

        public async Task<ResourceDTO> GetResourceById(int resourceId, int workspaceId)
        {
            var ws = await _context.Workspaces
                .AsNoTracking()
                .Include(ws => ws.Resources)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um Recurso com a ResourceId:{resourceId}.");
            return _mapper.Map<ResourceDTO>(rs);
        }

        public async Task<IEnumerable<TabDTO>> GetTabsByResourceId(int resourceId, int workspaceId)
        {
            var ws = await _context.Workspaces
                .Include(ws => ws.Resources)
                .ThenInclude(rs => rs.Tabs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            var rs = ws.Resources.SingleOrDefault(rs => rs.ResourceId == resourceId);
            var tabList = rs.Tabs.Select(tb => _mapper.Map<TabDTO>(tb));
            return tabList;
        }

        public async Task<ResourceDTO> CreateResource(PostResourceDTO resourceDTO)
        {
            var rs = new Resource()
            {
                Name = resourceDTO.Name,
                WorkspaceId = resourceDTO.WorkspaceId,
                Tabs = new List<Tab>()
            };
            await _context.Resources.AddAsync(rs);
            await _context.SaveChangesAsync();
            return _mapper.Map<ResourceDTO>(rs);
        }

        public async Task<ResourceDTO> UpdateResourceName(int resourceId, PutNameResourceDTO resourceDTO)
        {
            var rs = await _context.Resources.SingleOrDefaultAsync(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um Recurso com a ResourceId:{resourceId}.");
            rs.Name = resourceDTO.Name;
            await _context.SaveChangesAsync();
            return _mapper.Map<ResourceDTO>(rs);
        }

        public async Task<ResourceDTO> UpdateResourceWorkspaceId(int resourceId, int workspaceId, PutWorkspaceIdResourceDTO resourceDTO)
        {
            var rs = await _context.Resources.Where(rs => rs.WorkspaceId == workspaceId).SingleOrDefaultAsync(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um Recurso com a WorkspaceId:{workspaceId}.");
            rs.WorkspaceId = resourceDTO.WorkspaceId;
            await _context.SaveChangesAsync();
            return _mapper.Map<ResourceDTO>(rs);
        }

        public async Task DeleteResource(int resourceId)
        {
            var rs = await _context.Resources.SingleOrDefaultAsync(rs => rs.ResourceId == resourceId);
            if (rs == null) throw new NullReferenceException($"Não existe um Recurso com a ResourceId:{resourceId}.");
            _context.Resources.Remove(rs);
            await _context.SaveChangesAsync();
        }

    }

}
