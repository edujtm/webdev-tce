using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using AutoMapper;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public class JwtService : IJwtService
    {
        private readonly JwtSettings _jwtSettings;
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<JwtService> _log;

        public JwtService(
            AppDbContext context,
            JwtSettings jwtSettings,
            IMapper mapper,
            ILogger<JwtService> log
        )
        {
            _jwtSettings = jwtSettings;
            _context = context;
            _mapper = mapper;
            _log = log;
        }

        // TODO: Fazer um metodo separado para obter a refresh token
        public JwtTokenDTO GetTokenForUser(User user, string ipAddress)
        {
            var token = GenerateJwtToken(user);
            var refreshToken = GenerateRefreshToken(ipAddress);

            if (user.RefreshTokens == null)
            {
                user.RefreshTokens = new List<RefreshToken>() { refreshToken };
            }
            else
            {
                user.RefreshTokens.Add(refreshToken);
            }

            _context.Update(user);
            _context.SaveChanges();

            return new JwtTokenDTO()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = token.ValidTo,
                RefreshToken = _mapper.Map<RefreshTokenDTO>(refreshToken),
            };
        }

        public JwtTokenDTO RefreshToken(string token, string ipAddress)
        {
            var user = _context.Users
                .Include(user => user.RefreshTokens)
                .SingleOrDefault(user => user.RefreshTokens.Any(t => t.Token == token));
            if (user == null)
            {
                _log.LogError(
                    "Não foi possível encontrar usuário que possui a refresh token: {JwtToken}",
                    token
                );
                return null;
            }

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            if (!refreshToken.IsActive)
            {
                _log.LogWarning("Tentativa de atualizar uma token JWT com refresh token inativa");
                return null;
            }

            var newRefreshToken = GenerateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            user.RefreshTokens.Add(newRefreshToken);
            _context.Update(user);
            _context.SaveChanges();

            var newJwtToken = GenerateJwtToken(user);
            return new JwtTokenDTO()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(newJwtToken),
                Expiration = newJwtToken.ValidTo,
                RefreshToken = _mapper.Map<RefreshTokenDTO>(newRefreshToken),
            };
        }

        private RefreshToken GenerateRefreshToken(string ipAddress)
        {
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[64];
                cryptoProvider.GetBytes(bytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(bytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }

        private JwtSecurityToken GenerateJwtToken(User user)
        {
            var options = new IdentityOptions();
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Name, user.UserId.ToString())
            };

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            return new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                expires: DateTime.Now.AddMinutes(30),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );
        }
    }
}
