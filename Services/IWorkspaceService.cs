using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public interface IWorkspaceService
    {
        public Task<Workspace> GetWorkspaceById(int workspaceId);

        public Task<IEnumerable<ResourceDTO>> GetResourcesByWorkspaceId(int workspaceId);

        public Task<WorkspaceDTO> CreateWorkspace(int userId, PostWorkspaceDTO workspaceDTO);

        public Task<WorkspaceDTO> UpdateWorkspaceName(int workspaceId, PutNameWorkspaceDTO workspaceDTO);

        public Task DeleteWorkspace(int workspaceId);

        public Task<WorkspaceConfigDTO> CreateWorkspacePermission(PostWorkspaceConfigDTO workspaceConfigDTO);

        public Task<WorkspaceConfigDTO> UpdateWorkspacePermission(int workspaceId, PutAcessLevelWorkspaceConfigDTO workspaceConfigDTO);

        public Task DeleteWorkspacePermission(int workspaceId, DeleteWorkspaceConfigDTO workspaceConfigDTO);

    }
}
