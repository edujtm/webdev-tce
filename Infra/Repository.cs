using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace webdev_project.Infra
{
    /// <inheritdoc/>
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbSet<T> _dbSet;
        private readonly DbContext _context;

        public Repository(DbContext context, DbSet<T> dbSet)
        {
            _context = context;
            _dbSet = dbSet;
        }

        public T Add(T obj)
        {
            _dbSet.Add(obj);
            return obj;
        }

        public T Update(T obj)
        {
           _dbSet.Update(obj); 
           return obj;
        }

        public void DeleteAll(Expression<Func<T, bool>> predicate = null)
        {
            var objs = _dbSet as IQueryable<T>;
            if (predicate != null)
            {
                objs = _dbSet.Where(predicate);
            }

            if (objs == null)
            {
                throw new ArgumentException($"Não foi possível deletar objetos.");
            }

            _dbSet.RemoveRange(objs);
        }

        public void Delete(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("Não foi possível deletar objeto. Argumento nulo foi passado como parâmetro.");
            }
            _dbSet.Remove(obj);
        }

        public IEnumerable<T> GetAll(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null
        )
        {
            IQueryable<T> query = _dbSet;
            if (include != null)
            {
                query = include(query);
            }
            if (predicate != null)
            {
                query = query.Where(predicate);
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query.ToList();
        }

        public T Single(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate).SingleOrDefault();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
