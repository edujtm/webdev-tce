﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace webdev_project.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Data");

            migrationBuilder.CreateTable(
                name: "User",
                schema: "Data",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 30, nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Workspace",
                schema: "Data",
                columns: table => new
                {
                    WorkspaceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workspace", x => x.WorkspaceId);
                });

            migrationBuilder.CreateTable(
                name: "RefreshToken",
                schema: "Data",
                columns: table => new
                {
                    RefreshTokenId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatedByIp = table.Column<string>(nullable: true),
                    Revoked = table.Column<DateTime>(nullable: true),
                    RevokedByIp = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshToken", x => x.RefreshTokenId);
                    table.ForeignKey(
                        name: "FK_RefreshToken_User_UserId",
                        column: x => x.UserId,
                        principalSchema: "Data",
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Resource",
                schema: "Data",
                columns: table => new
                {
                    ResourceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    WorkspaceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resource", x => x.ResourceId);
                    table.ForeignKey(
                        name: "FK_Resource_Workspace_WorkspaceId",
                        column: x => x.WorkspaceId,
                        principalSchema: "Data",
                        principalTable: "Workspace",
                        principalColumn: "WorkspaceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkspaceConfig",
                schema: "Data",
                columns: table => new
                {
                    WorkspaceConfigId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    WorkspaceId = table.Column<int>(nullable: false),
                    AccessLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkspaceConfig", x => x.WorkspaceConfigId);
                    table.ForeignKey(
                        name: "FK_WorkspaceConfig_User_UserId",
                        column: x => x.UserId,
                        principalSchema: "Data",
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkspaceConfig_Workspace_WorkspaceId",
                        column: x => x.WorkspaceId,
                        principalSchema: "Data",
                        principalTable: "Workspace",
                        principalColumn: "WorkspaceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tab",
                schema: "Data",
                columns: table => new
                {
                    TabId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(maxLength: 256, nullable: false),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tab", x => x.TabId);
                    table.ForeignKey(
                        name: "FK_Tab_Resource_ResourceId",
                        column: x => x.ResourceId,
                        principalSchema: "Data",
                        principalTable: "Resource",
                        principalColumn: "ResourceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RefreshToken_UserId",
                schema: "Data",
                table: "RefreshToken",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_WorkspaceId",
                schema: "Data",
                table: "Resource",
                column: "WorkspaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Tab_ResourceId",
                schema: "Data",
                table: "Tab",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkspaceConfig_UserId",
                schema: "Data",
                table: "WorkspaceConfig",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkspaceConfig_WorkspaceId",
                schema: "Data",
                table: "WorkspaceConfig",
                column: "WorkspaceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RefreshToken",
                schema: "Data");

            migrationBuilder.DropTable(
                name: "Tab",
                schema: "Data");

            migrationBuilder.DropTable(
                name: "WorkspaceConfig",
                schema: "Data");

            migrationBuilder.DropTable(
                name: "Resource",
                schema: "Data");

            migrationBuilder.DropTable(
                name: "User",
                schema: "Data");

            migrationBuilder.DropTable(
                name: "Workspace",
                schema: "Data");
        }
    }
}
