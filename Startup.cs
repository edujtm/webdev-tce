using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;
using webdev_project.Infra;
using webdev_project.Services;
using webdev_project.Api.Extensions;
using webdev_project.Api.Filters;
using webdev_project.Api.Auth;


namespace webdev_project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(typeof(GlobalExceptionFilter));
            });

            // ------  Configuração EFCore  ------
            services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("NetCoreUserConnection")));

            //services.AddRepository<AppDbContext, Workspace>(context => context.Workspaces);
            //services.AddRepository<AppDbContext, User>(context => context.Users);

            // ------- Configuração de Services -------
            services.AddScoped<IJwtService, JwtService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IWorkspaceService, WorkspaceService>();
            services.AddScoped<IResourceService, ResourceService>();
            services.AddScoped<ITabService, TabService>();

            // ------- Configuração do AutoMapper -------
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // ------- Configuração do Swagger -------
            services.AddSwaggerConfiguration();

            // ------ Configuração da Autenticação -------
            var jwtSettings = Configuration.GetSection("JwtSettings").Get<JwtSettings>();
            services.AddSingleton(jwtSettings);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = jwtSettings.Issuer,
                    ValidIssuer = jwtSettings.Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(jwtSettings.Key)
                    ),
                };
            });

            services.AddCors(options => 
            {
                options.AddPolicy("Cors", builder =>
                {
                    builder.WithOrigins(
                            "http://localhost:4200",
                            "https://localhost:4200",
                            "http://localhost:5000",
                            "https://localhost:5001"
                        )
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("CanViewWorkspace", policyBuilder =>
                {
                    policyBuilder.AddRequirements(
                        new HasReadAccessRequirement()
                    );
                });

                options.AddPolicy("CanModifyWorkspace", policyBuilder =>
                {
                    policyBuilder.AddRequirements(
                        new CanModifyWorkspaceContentRequirement()
                    );
                });

                options.AddPolicy("IsWorkspaceOwner", policyBuilder =>
                {
                    policyBuilder.AddRequirements(
                        new IsWorkspaceOwnerRequirement()
                    );
                });
            });

            services.AddSingleton<IAuthorizationHandler, IsMemberOfWorkspaceHandler>();
            services.AddSingleton<IAuthorizationHandler, HasWorkspaceOwnerAccessLevelHandler>();
            services.AddSingleton<IAuthorizationHandler, HasReadWriteAccessToWorkspaceHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Webdev TCE API v1");
                    c.RoutePrefix = "";
                });
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("Cors");
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();


            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
