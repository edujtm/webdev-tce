using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

using webdev_project.Services;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Api.Controllers
{

    [Route("api/tab/")]
    [Authorize]
    [ApiController]
    public class TabController : Controller
    {
        private readonly IWorkspaceService _workspaceService;
        private readonly IResourceService _resourceService;
        private readonly ITabService _tabService;
        private readonly IAuthorizationService _authService;
        private readonly ILogger<TabController> _log;

        public TabController(
            IWorkspaceService workspaceService,
            IResourceService resourceService,
            ITabService tabService,
            IAuthorizationService authService,
            ILogger<TabController> log
        )
        {
            _workspaceService = workspaceService;
            _resourceService = resourceService;
            _tabService = tabService;
            _authService = authService;
            _log = log;
        }

        private int? GetUserIdFromClaims()
        {
            int? userId = null;
            try
            {
                var userIdClaim = User.Claims.First(c => c.Type == ClaimTypes.Name)?.Value;
                userId = int.Parse(userIdClaim);
            }
            catch (Exception)
            {
                _log.LogError("Não foi possível obter o id do usuário das tokens JWT.");
                return null;
            }
            return userId;
        }


        /// <summary>
        /// Retorna uma Tab pelo TabId.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona a Tab. \
        /// Variáveis do Body: Não possui. \
        /// Exemplo: \
        /// Route: GET /api/tab/1/resource/1/workspace/1 \
        /// Body:
        /// </remarks>
        [HttpGet("{tabId:int}/resource/{resourceId:int}/workspace/{workspaceId:int}")]
        [ProducesResponseType(typeof(TabDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetTabById([FromRoute] int tabId, [FromRoute] int resourceId, [FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanViewWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao ler o TabId:{tabId}");
                return Unauthorized();
            }

            try
            {
                var tb = await _tabService.GetTabById(tabId, resourceId, workspaceId);
                return Ok(tb);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Adiciona uma Tab.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: \
        /// Variáveis do Body: Define os parâmetros da nova Tab. \
        /// Exemplo: \
        /// Route: POST /api/tab/ \
        /// Body: {"Link":"google.com", "ResourceId":3, "WorkspaceId":2}
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(typeof(TabDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> CreateTab([FromBody] PostTabDTO tabDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(tabDTO.WorkspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{tabDTO.WorkspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao criar uma Tab no ResourceId:{tabDTO.ResourceId}");
                return Unauthorized();
            }
            try
            {
                var tb = await _tabService.CreateTab(tabDTO);
                return Ok(tb);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Edita o Link de uma Tab.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona a Tab. \
        /// Variáveis do Body: Define o Link de mudança. \
        /// Exemplo: \
        /// Route: PUT /api/tab/1/resource/4/workspace/1/link \
        /// Body: {"Link":"google.com"}
        /// </remarks>
        [HttpPut("{tabId:int}/resource/{resourceId:int}/workspace/{workspaceId:int}/link")]
        [ProducesResponseType(typeof(TabDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateTabLink([FromRoute] int tabId, [FromRoute] int resourceId, [FromRoute] int workspaceId, [FromBody] PutLinkTabDTO tabDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o TabId:{tabId}.");
                return Unauthorized();
            }

            try
            {
                var rs = await _tabService.UpdateTabLink(tabId, resourceId, workspaceId, tabDTO);
                return Ok(rs);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Edita os parentes de uma Tab.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona a Tab. \
        /// Variáveis do Body: Define os parâmetros da mudança. \
        /// Exemplo: \
        /// Route: PUT /api/tab/1/resource/4/workspace/1/resourceid \
        /// Body: {"ResourceId":2, "WorkspaceId":1}
        /// </remarks>
        [HttpPut("{tabId:int}/resource/{resourceId:int}/workspace/{workspaceId:int}/resourceid")]
        [ProducesResponseType(typeof(TabDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateTabResourceId([FromRoute] int tabId, [FromRoute] int resourceId, [FromRoute] int workspaceId, [FromBody] PutResourceIdTabDTO tabDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido.");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o TabId:{tabId}.");
                return Unauthorized();
            }
            workspace = await _workspaceService.GetWorkspaceById(tabDTO.WorkspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{tabDTO.WorkspaceId} não foi encontrado.");
                return NotFound();
            }
            authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o TabId:{tabId}.");
                return Unauthorized();
            }

            try
            {
                var rs = await _tabService.UpdateTabResourceId(tabId, resourceId, workspaceId, tabDTO);
                return Ok(rs);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Deleta uma Tab.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona a Tab. \
        /// Variáveis do Body: \
        /// Exemplo: \
        /// Route: GET /api/tab/1/resource/4/workspace/1/link \
        /// Body:
        /// </remarks>
        [HttpDelete("{tabId:int}/resource/{resourceId:int}/workspace/{workspaceId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteTab([FromRoute] int tabId, [FromRoute] int resourceId, [FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao deletar o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                await _tabService.DeleteTab(tabId, resourceId, workspaceId);
                return NoContent();
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }
    }
}