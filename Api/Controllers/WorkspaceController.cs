using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

using webdev_project.Services;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Api.Controllers
{
    [Route("api/workspace/")]
    [Authorize]
    [ApiController]
    public class WorkspaceController : Controller
    {

        private readonly IUserService _userService;
        private readonly IWorkspaceService _workspaceService;
        private readonly IAuthorizationService _authService;
        private readonly IMapper _mapper;
        private readonly ILogger<WorkspaceController> _log;

        public WorkspaceController(
            IUserService userService,
            IWorkspaceService workspaceService,
            IAuthorizationService authService,
            IMapper mapper,
            ILogger<WorkspaceController> log
        )
        {
            _userService = userService;
            _workspaceService = workspaceService;
            _authService = authService;
            _mapper = mapper;
            _log = log;
        }

        private int? GetUserIdFromClaims()
        {
            int? userId = null;
            try
            {
                var userIdClaim = User.Claims.First(c => c.Type == ClaimTypes.Name)?.Value;
                userId = int.Parse(userIdClaim);
            }
            catch (Exception)
            {
                _log.LogError("Não foi possível obter o id do usuário das tokens JWT.");
                return null;
            }
            return userId;
        }

        /// <summary>
        ///  Obtem informações sobre um workspace por ID.
        /// </summary>
        /// <remarks>
        /// O usuário deve possuir permissão de leitura do workspace.
        /// </remarks>
        [HttpGet("{workspaceId:int}")]
        [ProducesResponseType(typeof(WorkspaceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetWorkspaceById([FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanViewWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao ler o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            return Ok(new WorkspaceDTO()
            {
                WorkspaceId = workspace.WorkspaceId,
                Name = workspace.Name,
                AccessLevel = workspace.GetAccesLevelForUser((int)userId).ToString("g")
            });
        }

        /// <summary>
        ///  Obtem todos os recursos vinculados a esse workspace.
        /// </summary>
        /// <remarks>
        /// O usuário deve possuir permissão de leitura do workspace.
        /// </remarks>
        [HttpGet("{workspaceId:int}/resources")]
        [ProducesResponseType(typeof(IEnumerable<ResourceDTO>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetResourcesByWorkspaceId([FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanViewWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao ler o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                var result = await _workspaceService.GetResourcesByWorkspaceId(workspaceId);
                return Ok(result);
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cria um workspace para o usuário atual.
        /// </summary>
        /// <remarks>
        /// Qualquer usuário pode criar um Workspace como Owner.
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(typeof(WorkspaceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> CreateWorkspace([FromBody] PostWorkspaceDTO workspaceDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var ws = await _workspaceService.CreateWorkspace((int)userId, workspaceDTO);
            return Ok(ws);
        }

        /// <summary>
        /// Permite a alteração do nome do workspace.
        /// </summary>
        /// <remarks>
        /// Apenas o dono do workspace pode alterar seu nome.
        /// </remarks>
        [HttpPut("{workspaceId:int}/name")]
        [ProducesResponseType(typeof(WorkspaceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateWorkspaceName([FromRoute] int workspaceId, [FromBody] PutNameWorkspaceDTO workspaceDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "IsWorkspaceOwner");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                var ws = await _workspaceService.UpdateWorkspaceName(workspaceId, workspaceDTO);
                return Ok(ws);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Permite que um Workspace seja deletado
        /// </summary>
        /// <remarks>
        /// Apenas o dono do workspace pode deletá-lo.
        /// </remarks>
        [HttpDelete("{workspaceId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteWorkspace([FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "IsWorkspaceOwner");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                await _workspaceService.DeleteWorkspace(workspaceId);
                return NoContent();
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Dá permissão de acesso para um usuário a um workspace.
        /// </summary>
        /// <remarks>
        /// As permissões possíveis são ReadOnly e ReadWriteAccess e devem ser \
        /// setadas de forma case sensitive em acessLevel. \
        /// Exemplo: \
        /// POST /api/workspace/1/permissions \
        /// { \
        ///   userId: 1, \
        ///   acessLevel: "ReadOnly" \
        /// }
        /// </remarks>
        [HttpPost("permissions")]
        [ProducesResponseType(typeof(WorkspaceConfigDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> CreateWorkspacePermission([FromBody] PostWorkspaceConfigDTO workspaceConfigDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceConfigDTO.WorkspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceConfigDTO.WorkspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "IsWorkspaceOwner");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao adicionar permissão no WorkspaceId:{workspaceConfigDTO.WorkspaceId}");
                return Unauthorized();
            }

            try
            {
                var wsc = await _workspaceService.CreateWorkspacePermission(workspaceConfigDTO);
                return Ok(wsc);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Altera a permissão de um usuário em relação a um workspace.
        /// </summary>
        /// <remarks>
        /// As permissões possíveis são ReadOnly e ReadWriteAccess e devem ser \
        /// setadas de forma case sensitive em acessLevel. \
        /// Exemplo: \
        /// PUT /api/workspace/1/permissions \
        /// { \
        ///   userId: 1, \
        ///   acessLevel: "ReadOnly" \
        /// }
        /// </remarks>
        [HttpPut("{workspaceId:int}/permissions")]
        [ProducesResponseType(typeof(WorkspaceConfigDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateWorkspacePermission([FromRoute] int workspaceId, [FromBody] PutAcessLevelWorkspaceConfigDTO workspaceConfigDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "IsWorkspaceOwner");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao modificar permissão no WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                var wsc = await _workspaceService.UpdateWorkspacePermission(workspaceId, workspaceConfigDTO);
                return Ok(wsc);
            }
            catch (NullReferenceException) { return BadRequest(); }
            catch (ArgumentException) { return BadRequest(); }
        }

        /// <summary>
        /// Altera a permissão de um usuário em relação a um workspace.
        /// </summary>
        /// <remarks>
        /// As permissões possíveis são ReadOnly e ReadWriteAccess e devem ser \
        /// setadas de forma case sensitive em acessLevel. \
        /// Exemplo: \
        /// PUT /api/workspace/1/permissions \
        /// { \
        ///   userId: 1, \
        ///   acessLevel: "ReadOnly" \
        /// }
        /// </remarks>
        [HttpDelete("{workspaceId:int}/permissions")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteWorkspacePermission([FromRoute] int workspaceId, [FromBody] DeleteWorkspaceConfigDTO workspaceConfigDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "IsWorkspaceOwner");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao deletar permissão no WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                await _workspaceService.DeleteWorkspacePermission(workspaceId, workspaceConfigDTO);
                return NoContent();
            }
            catch (NullReferenceException) { return BadRequest(); }
            catch (ArgumentException) { return BadRequest(); }
        }
    }
}
