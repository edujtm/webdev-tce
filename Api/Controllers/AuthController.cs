using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using AutoMapper;

using webdev_project.Services;
using webdev_project.Domain.Models;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;
using webdev_project.Api.Filters;

namespace webdev_project.Api.Controllers
{
    [Route("api/auth")]
    [AllowAnonymous]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IAuthService _authService;
        private readonly IJwtService _jwtService;
        private readonly ILogger<AuthController> _log;
        private readonly IMapper _mapper;

        public AuthController(
            IAuthService authService,
            IJwtService jwtService,
            ILogger<AuthController> log,
            IMapper mapper
        )
        {
            _authService = authService;
            _jwtService = jwtService;
            _log = log;
            _mapper = mapper;
        }

        /// <summary>
        /// Faz o login do usuário e retorna uma token JWT utilizada para
        /// fazer autorização em endpoints protegidos.
        /// </summary>
        /// <param name="loginDTO">Informações necessárias para login: nome de usuário e senha.</param>
        /// <remarks>
        ///  Além da acess token, uma refresh token é armazenada nos cookies para que seja possível
        ///  obter novas tokens JWT sem utilizar as credenciais do usuário.
        /// </remarks>
        [HttpPost("login")]
        [ProducesResponseType(typeof(AuthInfoDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Login([FromBody] UserLoginDTO loginDTO)
        {
            var user = _authService.FindUserByName(loginDTO.UserName);
            if (user != null && _authService.CheckPassword(user, loginDTO.Password))
            {
                var ipAddress = GetCurrentIpAddress();
                var jwtToken = _jwtService.GetTokenForUser(user, ipAddress);
                SetTokenCookie(jwtToken.RefreshToken.Token);
                _log.LogInformation("Usuário com username {UserName} logado com sucesso.", loginDTO.UserName);
                var authInfo = new AuthInfoDTO()
                {
                    TokenInfo = jwtToken,
                    UserInfo = _mapper.Map<UserInfoDTO>(user)
                };
                return Ok(authInfo);
            }

            var message = user == null ? "Não foi possível encontrar usuário com username: {UserName}"
                : "Usuário com username {UserName} informou senha errada.";
            _log.LogWarning(message, loginDTO.UserName);

            return Unauthorized();
        }

        /// <summary>
        /// Cria um novo usuário e retorna uma token JWT para autenticação
        /// em endpoints protegidos.
        /// </summary>
        /// <param name="userCreateDTO">Infomações necessárias para cadastro do usuário.</param>
        /// <remarks>Retorna os mesmos dados do endpoint de login, para evitar uma segunda requisição</remarks>
        [HttpPost("sign-up")]
        [EnsurePasswordConfirmationMatch]
        [ProducesResponseType(typeof(JwtTokenDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult SignUp([FromBody] UserCreateDTO userCreateDTO)
        {
            var user = new User()
            {
                UserName = userCreateDTO.UserName,
                Email = userCreateDTO.Email,
            };
            var result = _authService.CreateUser(user, userCreateDTO.Password);

            switch (result)
            {
                case Success<User> s:
                    var ipAddress = GetCurrentIpAddress();
                    var jwtToken = _jwtService.GetTokenForUser(s.Data, ipAddress);
                    SetTokenCookie(jwtToken.RefreshToken.Token);
                    _log.LogInformation(
                        "Usuário com username {UserName} criado com sucesso.",
                        userCreateDTO.UserName
                    );

                    var authInfo = new AuthInfoDTO()
                    {
                        TokenInfo = jwtToken,
                        UserInfo = _mapper.Map<UserInfoDTO>(user)
                    };
                    return Ok(authInfo);
                case Error e:
                    foreach (var error in e.Errors)
                    {
                        _log.LogWarning($"Erro ao criar usuário: {error.Message}");
                        ModelState.AddModelError("UserName", error.Message);
                    }
                    return BadRequest(ModelState);
                default:
                    _log.LogError("Resultado do sign up não foi Success ou Error. Isto não deveria acontecer.");
                    return Unauthorized();
            }
        }

        /// <summary>
        /// Permite obter uma nova token JWT quando a token antiga expirar.
        /// </summary>
        /// <remarks>
        ///  É necessário apenas fazer um GET neste endpoint com a refresh token
        ///  armazenada nos cookies. A token é armazenada nos cookies ao fazer login
        ///  ou sign-up.
        /// </remarks>
        [HttpGet("refresh-token")]
        [ProducesResponseType(typeof(JwtTokenDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var ipAddress = GetCurrentIpAddress();
            var jwtToken = _jwtService.RefreshToken(refreshToken, ipAddress);

            if (jwtToken == null)
            {
                _log.LogWarning("Não foi possível refrescar token JWT.");
                return Unauthorized();
            }

            SetTokenCookie(jwtToken.RefreshToken.Token);
            return Ok(jwtToken);
        }

        private void SetTokenCookie(string refreshToken)
        {
            var cookieOptions = new CookieOptions()
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", refreshToken, cookieOptions);
        }

        private string GetCurrentIpAddress()
        {
            // Se a requisição vier de um reverse proxy
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                return Request.Headers["X-Forwarded-For"];
            }
            else
            {
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            }
        }
    }
}
