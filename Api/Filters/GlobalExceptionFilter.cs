
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace webdev_project.Api.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<GlobalExceptionFilter> _log;
        public GlobalExceptionFilter(ILogger<GlobalExceptionFilter> log)
        {
            _log = log;
        }

        public void OnException(ExceptionContext context)
        {
            if (!context.ExceptionHandled)
            {
                _log.LogError(
                    "Ocorreu uma exceção não tradada: {ErrorMessage}", 
                    context.Exception.Message
                );
                context.Result = new StatusCodeResult(500);
                context.ExceptionHandled = true;
            }
        }
    }
}
