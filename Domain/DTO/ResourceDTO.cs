using System.ComponentModel.DataAnnotations;

namespace webdev_project.Domain.DTO
{
    public class ResourceDTO
    {
        [Required]
        public int ResourceId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        public int WorkspaceId { get; set; }
    }

    public class PostResourceDTO
    {
        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        public int WorkspaceId { get; set; }
    }

    public class PutNameResourceDTO
    {
        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string Name { get; set; }
    }

    public class PutWorkspaceIdResourceDTO
    {
        [Required]
        public int WorkspaceId { get; set; }
    }
}