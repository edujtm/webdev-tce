using System.ComponentModel.DataAnnotations;

namespace webdev_project.Domain.DTO
{
    public class UserLoginDTO
    {
        [MinLength(4)]
        [MaxLength(30)]
        public string UserName { get; set; }

        [MinLength(8)]
        public string Password { get; set; }
    }
}
