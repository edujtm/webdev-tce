using System.ComponentModel.DataAnnotations;

using webdev_project.Domain.Models;

namespace webdev_project.Domain.DTO
{
    public class WorkspaceConfigDTO
    {
        [Required]
        public int WorkspaceConfigId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int WorkspaceId { get; set; }

        [Required]
        public string AccessLevel { get; set; }
    }

    public class PostWorkspaceConfigDTO
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int WorkspaceId { get; set; }

        [Required]
        public string AccessLevel { get; set; }
    }

    public class PutAcessLevelWorkspaceConfigDTO
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string AccessLevel { get; set; }
    }

    public class DeleteWorkspaceConfigDTO
    {
        [Required]
        public int UserId { get; set; }
    }
}
