using System;
using System.Text.Json.Serialization;

namespace webdev_project.Domain.DTO
{
    public class JwtTokenDTO
    {
        public string Token { get; set; }

        public DateTime Expiration { get; set; }

        [JsonIgnore]
        public RefreshTokenDTO RefreshToken { get; set; }
    }
}
