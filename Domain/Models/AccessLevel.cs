namespace webdev_project.Domain.Models
{
    /// <summary>
    /// Define o nivel de acesso de um usuário membro de um workspace.
    /// </summary>
    public enum AccessLevel
    {
        /// <summary>
        /// Tem acesso apenas para visualizar os itens do workspace.
        /// </summary>
        ReadOnly = 1,

        /// <summary>
        /// Um membro com permissão de leitura e escrita pode adicionar e remover
        /// itens do workspace, mas não pode deletar o workspace ou remover membros.
        /// </summary>
        ReadWriteAccess = 2,

        ///<summary>
        /// O dono do workspace pode deletar o workspace 
        /// e remover outros usuários, além das funcionalidades pertencentes à outros 
        /// niveis de acesso.
        ///</summary>
        Owner = 3
    }
}
