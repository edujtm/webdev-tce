using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using webdev_project.Domain.Models;

namespace webdev_project.Domain.Entities
{
    [Table("WorkspaceConfig", Schema = "Data")]
    public class WorkspaceConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkspaceConfigId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int WorkspaceId { get; set; }

        [Required]
        public AccessLevel AccessLevel { get; set; }

        // ------- Relacionamentos -------

        public Workspace Workspace { get; set; }
        public User Member { get; set; }
    }
}
