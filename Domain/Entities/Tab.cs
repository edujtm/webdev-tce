using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webdev_project.Domain.Entities
{
    [Table("Tab", Schema = "Data")]
    public class Tab
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TabId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Link { get; set; }

        [Required]
        public int ResourceId { get; set; }

        public Resource Resource { get; set; }
    }
}