using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using webdev_project.Domain.Models;

namespace webdev_project.Domain.Entities
{
    [Table("Workspace", Schema = "Data")]
    public class Workspace
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkspaceId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(100)]
        public string Name { get; set; }

        public ICollection<WorkspaceConfig> WorkspaceConfigs { get; set; }

        public ICollection<Resource> Resources { get; set; }

        public bool HasMember(int userId)
        {
            return this.WorkspaceConfigs.Any(config => config.UserId == userId);
        }

        public WorkspaceConfig GetConfigForUser(int userId)
        {
            return this.WorkspaceConfigs.SingleOrDefault(config => config.UserId == userId);
        }

        public AccessLevel GetAccesLevelForUser(int userId)
        {
            return this.WorkspaceConfigs.SingleOrDefault(config => config.UserId == userId).AccessLevel;
        }

        public static Workspace CreateWithOwner(string name, int userId)
        {
            var wsc = new WorkspaceConfig()
            {
                UserId = userId,
                AccessLevel = AccessLevel.Owner
            };
            var ws = new Workspace()
            {
                Name = name,
                WorkspaceConfigs = new List<WorkspaceConfig>() { wsc }
            };

            return ws;
        }
    }
}
