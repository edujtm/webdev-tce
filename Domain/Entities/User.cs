using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webdev_project.Domain.Entities
{
    [Table("User", Schema = "Data")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string UserName { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public ICollection<WorkspaceConfig> WorkspaceConfigs { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
