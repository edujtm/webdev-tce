using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webdev_project.Domain.Entities
{
    [Table("Resource", Schema = "Data")]
    public class Resource
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ResourceId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        public int WorkspaceId { get; set; }

        public Workspace Workspace { get; set; }

        public ICollection<Tab> Tabs { get; set; }
    }
}