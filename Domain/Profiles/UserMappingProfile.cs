using AutoMapper;

using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;

namespace webdev_project.Domain.Profiles
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserInfoDTO>();
        }
    }
}
