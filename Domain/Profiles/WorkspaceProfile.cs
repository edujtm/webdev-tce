using AutoMapper;

using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;

namespace webdev_project.Domain.Profiles
{
    public class WorkspaceProfile : Profile
    {
        public WorkspaceProfile()
        {
            CreateMap<Workspace, WorkspaceItemDTO>();
            CreateMap<Resource, ResourceDTO>();
            CreateMap<WorkspaceConfig, WorkspaceConfigDTO>();
            CreateMap<PostWorkspaceConfigDTO, WorkspaceConfig>();
            CreateMap<PostTabDTO, Tab>();
            CreateMap<Tab, TabDTO>();
        }
    }
}
